namespace api.Models.Request;

public class ClassSpellRequest
{
    public string ClassName { get; set; }
    public int    Level { get; set; }
}
