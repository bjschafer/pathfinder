namespace api.Models.Response;

public class ClassSpellResponse
{
    public string ClassName { get; set; }
    public int    Level { get; set; }

}
